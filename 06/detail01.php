<!DOCTYPE html>
<link rel="stylesheet" href="./include/style.css">
 <?php
  // common
  // include("./include/functions.php");
  $DB_DSN = "mysql:host=localhost; dbname=kurata_kensyu; charset=utf8";
  $DB_USER = "php_kurata";
  $DB_PW = "O3Xem9bsKU0RSb4D";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // $pdo = initDB();

  include("./include/statics.php");
  $query_str = "SELECT
               member.id AS ai,
	             member.name AS namae,
               member.sex AS se,
               member.age AS ag,
               member.ward AS wa,
               grade_master.name AS grd,
               section_master.name AS sec
               FROM member
               LEFT JOIN section_master ON section_master.id = member.section
               LEFT JOIN grade_master ON grade_master.id = member.grade
               WHERE member.id = " . $_GET['id'];

  // $query_str = "SELECT * FROM `test_table` WHERE dish_name LIKE '%の%' AND genre = 'おつまみ'";

  //echo $query_str;

  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
  //var_dump($result);

 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員情報</title>
    <style>
    .table1 {
      border: 1px solid gray;
    }
    .table1 th, .table1 td {
      border: 1px solid gray;
    }
    .table1 th {
      background-color: #c0c0c0;
    }
    .table1 { width: 300px;
    }

    .rink{
      text-align: right ;
    }

    </style>

    <script>
       function conf(){
         if(window.confirm('削除してよろしいですか？')){
           document.del_worker.submit();
          }else{
           return false;
          }
      }
    </script>

  </head>
  <body>
   <h1>社員名簿システム</h1>
   <br/>
   <a href="index.php">トップ画面へ</a>
   <a href="entry01.php">新規社員登録へ</a>
   <hr/>

<pre>
  <?php //var_dump($result); ?>
</pre>



    <table class="table1">
      <tr>
        <th>
          社員ID
        </th>
          <td>
            <?php echo $result[0]['ai']; ?>
          </td>
      </tr>
      <tr>
        <th>
          名前
        </th>
         <td>
            <?php echo $result[0]['namae']; ?>
         </td>
      </tr>
      <tr>
        <th>
          出身地
        </th>
         <td>
            <?php echo $pref_array[$result[0]['wa']]; ?>
            <?php // echo $pref_array['0']; ?>
         </td>
      </tr>
      <tr>
        <th>
          性別
        </th>
         <td>
            <?php echo $gender_array[$result[0]['se']]; ?>
         </td>
      </tr>
      <tr>
        <th>
          年齢
        </th>
         <td>
             <?php echo $result[0]['ag']; ?>
         </td>
      </tr>
      <tr>
        <th>
          部署
        </th>
         <td>
           <?php echo $result[0]['sec']; ?>
        </td>
      </tr>
      <tr>
        <th>
          役職
        </th>
         <td>
           <?php echo $result[0]['grd']; ?>
         </td>
      </tr>
    </table>
    <form method="POST" action="entry_update01.php">
     <input type="submit" value="編集">
     <input type="hidden" name="id" value="<?php echo $result[0]['ai']?>">
   </form>

     <form method="POST" action="delete01.php" name="del_worker">
     <input type="button" value="削除" onClick="conf();">
     <input type="hidden" name="id02" value="<?php echo $result[0]['ai']?>">
    </form>
   </body>

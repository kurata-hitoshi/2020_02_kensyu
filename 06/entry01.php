<html>
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>社員名簿</title>
    <style>
    .table1 {
      border: 1px solid gray;
    }
    .table1 th, .table1 td {
      border: 1px solid gray;
    }
    .table1 th {
      background-color: #c0c0c0;
    }
    .table1 { width: 600px;
    }

    .rink{
      text-align: right ;
    }

    </style>

    <script>
      function conf(){
          if(window.confirm('登録を完了してよろしいですか？')){
            document.entry_form.submit();
          }else{
            return false;
          }
      }
     </script>

    <?php
    include("./include/statics.php");
    ?>
 </head>
 <body>
     <h1>社員名簿システム</h1>
     <br/>
     <a href="index.php">トップ画面へ</a>
     <a href="entry01.php">新規社員登録へ</a>
     <hr/>
   <table class="table1">
   <form method="POST" action="entry02.php" name="entry_form">
    <tr>
      <th>
         名前
      </th>
        <td>
          <input type="text" name="name02">
        </td>
    </tr>
      <th>
         出身地
      </th>
        <td>
          <select name="ward01">
            <option value=""selected>都道府県</option>
            <?php
            foreach($pref_array as $key => $value){
              echo "<option value='" . $key . "'>" . $value . "</option>";
            }
            ?>
          </select>
        </td>
    </tr>
    <tr>
      <th>
        性別
      </th>
        <td>
          <input type="radio" name="gender01" value="1"checked>男
          <input type="radio" name="gender01" value="2">女
        </td>
    </tr>
    <tr>
      <th>
        年齢
      </th>
        <td>
          <input type="text" name="age01">
        </td>
    </tr>
    <tr>
       <th>
        所属部署
      </th>
       <td>
        <input type="radio" name="section01" value="1"checked>第一事業部
        <input type="radio" name="section01" value="2">第二事業部
        <input type="radio" name="section01" value="3">営業
        <input type="radio" name="section01" value="4">総務
        <input type="radio" name="section01" value="5">人事
       </td>
    </tr>
    <tr>
      <th>
        役職
      </th>
        <td>
          <input type="radio" name="grade01" value="1"checked>事業部長
          <input type="radio" name="grade01" value="2">部長
          <input type="radio" name="grade01" value="3">チームリーダー
          <input type="radio" name="grade01" value="4">リーダー
          <input type="radio" name="grade01" value="5">メンバー
        </td>
    </tr>
  </table>
  <br/>
          <input type="button" value="登録" onClick="conf();">
          <input type="reset" value="リセット">
   </form>
 </body>
</html>

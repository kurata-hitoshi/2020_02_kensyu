
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>消費税計算ページ</title>
    <link rel="stylesheet" href="style.css">
   <style>
   .table1 {
 border: 1px solid gray;
}
.table1 th, .table1 td {
 border: 1px solid gray;
}
    </style>
  </head>
  <body>
    <form method="POST" action="tax.php">
      <table class="table1">
        <tr>
          <th>
            商品名
          </th>
          <th>
            価格
          </th>
          <th>
            個数
          </th>
          <th>
            税率
          </th>
        </tr>
        <tr>
          <td>
            <input type="text" name="shouhin01">
          </td>
          <td>
            <input type="text" name="kakaku01">
          </td>
          <td>
            <input type="number" name="kosuu01">
          </td>
          <td>
            <input type="radio" name="zeiritsu01" value="1.08"checked>8%
            <input type="radio" name="zeiritsu01" value="1.1">10%
          </td>
        </tr>
        <tr>
          <td>
            <input type="text" name="shouhin02">
          </td>
          <td>
            <input type="text" name="kakaku02">
          </td>
          <td>
            <input type="number" name="kosuu02">
          </td>
          <td>
            <input type="radio" name="zeiritsu02" value="1.08"checked>8%
            <input type="radio" name="zeiritsu02" value="1.1">10%
          </td>
        </tr>
        <tr>
          <td>
            <input type="text" name="shouhin03">
          </td>
          <td>
            <input type="text" name="kakaku03">
          </td>
          <td>
            <input type="number" name="kosuu03">
          </td>
          <td>
            <input type="radio" name="zeiritsu03" value="1.08"checked>8%
            <input type="radio" name="zeiritsu03" value="1.1">10%
          </td>
        </tr>
        <tr>
          <td>
            <input type="text" name="shouhin04">
          </td>
          <td>
            <input type="text" name="kakaku04">
          </td>
          <td>
            <input type="number" name="kosuu04">
          </td>
          <td>
            <input type="radio" name="zeiritsu04" value="1.08"checked>8%
            <input type="radio" name="zeiritsu04" value="1.1">10%
          </td>
        </tr>
        <tr>
          <td>
            <input type="text" name="shouhin05">
          </td>
          <td>
            <input type="text" name="kakaku05">
          </td>
          <td>
            <input type="number" name="kosuu05">
          </td>
          <td>
            <input type="radio" name="zeiritsu05" value="1.08"checked>8%
            <input type="radio" name="zeiritsu05" value="1.1">10%
          </td>
        </tr>
  </table>
  <tr>
    <td>
    </td>
    <td>
      <input type="submit" value=" 送信 ">
      <input type="reset" value=" 取り消し ">
    </td>
  </tr>
    </form>
  </body>

  <hr>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
    <link rel="stylesheet" href="style.css">
    <style>
   .table1 {
 border: 1px solid gray;
}
.table1 th, .table1 td {
 border: 1px solid gray;
}
    </style>
  </head>
  <body>
    <form method="POST" action="tax.php">
      <table class="table1">
        <tr>
          <th>
            商品名
          </th>
          <th>
            価格
          </th>
          <th>
            個数
          </th>
          <th>
            税率
          </th>
          <th>
            小計
          </th>
        </tr>
        <tr>
          <td>
            <?php
              echo $_POST['shouhin01'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku01'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kosuu01'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['zeiritsu01'];
            ?>
          </td>
          <td>
            <?php
               $price = $_POST['kakaku01'];
               $price = $price * $_POST['kosuu01'] * $_POST['zeiritsu01'];
               echo $price . "円　（税込）";
               $total = 0;
               $total = $total + $price;
            ?>
          </td>
        </tr>

        <tr>
          <td>
            <?php
              echo $_POST['shouhin02'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku02'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kosuu02'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['zeiritsu02'];
            ?>
          </td>
          <td>
            <?php
              $price = $_POST['kakaku02'];
              $price = $price * $_POST['kosuu02'] * $_POST['zeiritsu02'];
              echo $price . "円　（税込）";
              $total = $total + $price;
            ?>
          </td>
        </tr>

        <tr>
          <td>
            <?php
              echo $_POST['shouhin03'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku03'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kosuu03'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['zeiritsu03'];
            ?>
          </td>
          <td>
            <?php
               $price = $_POST['kakaku03'];
               $price = $price * $_POST['kosuu03'] * $_POST['zeiritsu03'];
               echo $price . "円　（税込）";
               $total = $total + $price;
            ?>
          </td>
        </tr>

        <tr>
          <td>
            <?php
              echo $_POST['shouhin04'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku04'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kosuu04'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['zeiritsu04'];
            ?>
          </td>
          <td>
            <?php
               $price = $_POST['kakaku04'];
               $price = $price * $_POST['kosuu04'] * $_POST['zeiritsu04'];
               echo $price . "円　（税込）";
               $total = $total + $price;
            ?>
          </td>
        </tr>

        <tr>
          <td>
            <?php
              echo $_POST['shouhin05'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku05'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['kosuu05'];
            ?>
          </td>
          <td>
            <?php
              echo $_POST['zeiritsu05'];
            ?>
          </td>
          <td>
            <?php
              $price = $_POST['kakaku05'];
              $price = $price * $_POST['kosuu05'] * $_POST['zeiritsu05'];
              echo $price . "円　（税込）";
              $total = $total + $price;
            ?>
          </td>
        </tr>
        <tr>
         <th>合計</th>
          <td>
            <?php
               echo $total . "円　（税込）";
            ?>
          </td>
        </tr>



      </table>

    </form>
  </body>
</html>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ループのページ</title>
    <link rel="stylesheet" href="style.css">
    <style>
      .table1 {
        border: 1px solid gray;
      }
      .table1 th, .table1 td {
        border: 1px solid gray;
      }
    </style>
  </head>

  <body>
    <form method="POST" action="loop02.php">
      <input type="text" name="number01">
      行のテーブルを作成する
      <br/>
      <br/>
      <input type="submit" value=" 送信 ">
      <input type="reset" value=" 取消 ">
    </form>
  </body>

  <hr>

  <body>
    <table class="table1">
        <?php
        for( $i = 0 ; $i < $_POST['number01'] ; $i ++ ){
          echo "<tr><th>gorira</th><th>tanuki</th><th>neko</th></tr>";
        }
        ?>
    </table>
 </body>
</html>
